def sum_summables(dictionary):
    total = 0
    for k in dictionary:
        if type(dictionary[k]) == float:
            total += dictionary[k]
        elif type(dictionary[k]) == str and dictionary[k].isnumeric():
            total += float(dictionary[k])
        elif type(dictionary[k]) == int:
            total += dictionary[k]
        elif dictionary[k] == True:
            total += 1
    return total

dictionary = {
    "dog": 1,
    "number": "three",
    "size": "2",
    "heavy": True,
    "weight": 3.4,
}
total = sum_summables(dictionary)
print(total) # --> 7.4


print(isinstance(1, int))       # --> True
print(isinstance("cat", str))   # --> True
print(isinstance(True, int))    # --> True, what?! Did you know this?
print(isinstance("cat", int))   # --> False
print(isinstance(1, str))       # --> False
