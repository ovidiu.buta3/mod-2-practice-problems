def third_largest_value(values):
    if len(values) < 3:
        return None
    values.sort()
    return values[-3]
