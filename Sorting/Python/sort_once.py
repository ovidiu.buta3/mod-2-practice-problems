def sort(items):
    for i in range(len(items)-1):
        if items[i] > items[i+1]:
            (items[i], items[i+1]) = (items[i+1], items[i])
        return items
