function median(numbers) {
    numbers.sort((a,b) => a-b);
    if (numbers.length % 2 === 0){
        return (numbers[numbers.length/2 - 1] + numbers[numbers.length/2])/2
    } else {
        return numbers[Math.floor(numbers.length/2)]
    }
}
