def old_guzzlers(cars):
    return [car for car in cars if car["year"]<1980 and car["mpg"]<12]
