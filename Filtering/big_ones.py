def big_ones(cars):
    return [car for car in cars if car["length"] > 25 or car["max_weight"] >4000]
