function countWords(str) {
    if (str === ""){
        return {}
    }
  const obj = {};
  const words = str.split(" ");
  for (let w of words){
      if (obj[w] === undefined){
          obj[w] = 1;
      } else {
          obj[w] += 1;
      }
  } return obj;
}
